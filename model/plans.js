// require modules
var mongoose        = require('mongoose');

var uniqueValidator = require('mongoose-unique-validator');

var textSearch      = require("mongoose-text-search");

//schema for plans
var PlanSchema = new mongoose.Schema({
  schemeCode      : { type: Number, required: true, unique: true },
  payout          : { type: String, required: true },
  reinvestment    : { type: String, required: true },
  schemeName      : { type: String, required: true },
  netAssetValue   : { type: Number, required: true },
  repurchasePrice : { type: Number, required: true },
  salePrice       : { type: Number, required: true },
  planCreatedDate : { type: String, required: true },
  createdAt       : { type: Date, default: Date.now },
  updatedAt       : { type: Date, default: null },
  schemeId        : { type: String, ref: 'SchemeType', required: true },
  fundId          : { type: String, ref: 'Fund', required: true }
});

// adding plugins to mongoose
PlanSchema.plugin(uniqueValidator);

PlanSchema.plugin(textSearch);

// adding mongo full text search index
PlanSchema.index({
  schemeCode    : 'text',
  payout        : 'text',
  reinvestment  : 'text',
  schemeName    : 'text'
}, {
  name: "best_match_index",
  weights: {
    schemeCode  : 5,  
    schemeName  : 4
  }
});

//export plan schema
module.exports = mongoose.model('Plan', PlanSchema);
