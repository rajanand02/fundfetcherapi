// require modules
var mongoose        = require('mongoose');

var uniqueValidator = require('mongoose-unique-validator');

var textSearch      = require("mongoose-text-search");

// schema for schemetypes
var SchemeTypeSchema = new mongoose.Schema({
  name      : { type: String, required: true, unique: true },
  fundCount : { type: Number, default: 0 },
  createdAt : { type: Date, default: Date.now },
  updatedAt : { type: Date, default: null }
});

// adding mongoose plugins
SchemeTypeSchema.plugin(uniqueValidator);
SchemeTypeSchema.plugin(textSearch);

// adding search index
SchemeTypeSchema.index({
  name: 'text'
}, {
  name: "best_match_index",
  weights : {
    name  : 4
  }
});

//export scheme schema
module.exports = mongoose.model('SchemeType', SchemeTypeSchema);
