// require modules
var mongoose        = require('mongoose');

var uniqueValidator = require('mongoose-unique-validator');

var textSearch      =  require("mongoose-text-search");

// mongoose schema for fund collection
var FundSchema = new mongoose.Schema({
  planCount : { type: Number, default: 0 },
  createdAt : { type: Date, default: Date.now },
  updatedAt : { type: Date, default: null },
  schemeId  : { type: String, ref: 'SchemeType', required: true },
  name      : { type: String, required: true, unique: true }
});

// adding plugins
FundSchema.plugin(uniqueValidator);

FundSchema.plugin(textSearch);

// adding mongodb full text search index
FundSchema.index({
  name: 'text'
}, {
  name: "best_match_index",
  weights : {
    name  : 4
  }
});

// export fund schema
module.exports = mongoose.model('Fund', FundSchema);
