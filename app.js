// require libraries
var lineByLine    = require('linebyline');
var mongoose      = require('mongoose');

// require model
var SchemeType    = require('./model/schemeType.js');
var Fund          = require('./model/fund.js');
var Plan          = require('./model/plans.js');

// connect to database
mongoose.connect('mongodb://localhost:27017/fund_finder');

//read line by line
var readline = lineByLine('./tax.txt');

// temp object to store schemeId and fundId
var lineObj = {};

readline.on('line', function (line) {

    var lineArray = line.split(';');
  // if length is one then it must be either schemeType of Fund 
  if (lineArray.length === 1) {
    
    //  if scheme, create schemeType document
    if (lineArray[0].search('Schemes') > -1) {

      var schemeType = new SchemeType();
      schemeType.name   = lineArray[0];
      lineObj.schemeId  = schemeType._id;
      // should ideally use findAndModify and handle the error. Using save for time being.
      schemeType.save();
    } else if (lineArray[0].search('Mutual Fund') > -1) {
      var fund = new Fund();
      fund.name       = lineArray[0].trim();
      fund.schemeId   = lineObj.schemeId;
      lineObj.fundId  = fund._id;
      fund.save();
    }
  } else {
    // if length is greater than one then create plans
    var plan = new Plan();
    plan.schemeCode       = lineArray[0];
    plan.payout           = lineArray[1];
    plan.reinvestment     = lineArray[2];
    plan.schemeName       = lineArray[3];
    plan.netAssetValue    = lineArray[4];
    plan.repurchasePrice  = lineArray[5];
    plan.salePrice        = lineArray[6];
    plan.planCreatedDate  = lineArray[7];
    plan.schemeId         = lineObj.schemeId;
    plan.fundId           = lineObj.fundId;
    plan.save();
  }
})
.on('error', function (e) {
  console.log("Something went Wrong");
})
.on('close', function () {
  console.log("Database updated successfully..");
  });
